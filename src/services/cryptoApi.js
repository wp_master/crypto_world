import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
//Define API Headers
const cryptoApiHeaders = {
  "x-rapidapi-host": "coinranking1.p.rapidapi.com",
  "x-rapidapi-key": "bd82279cb0mshc229212cab5cdcdp151b5fjsnf93b932e464f",
};
//base URL for rapid API
const baseUrl = "https://coinranking1.p.rapidapi.com";

const createRequest = (url) => ({ url, headers: cryptoApiHeaders });
const createTimeperoidRequest = (url, params) => ({
  url,
  headers: cryptoApiHeaders,
  params: params,
});

//API query Configuration
export const cryptoApi = createApi({
  reducerPath: "cryptoApi",
  baseQuery: fetchBaseQuery({ baseUrl }),
  endpoints: (builder) => ({
    getCryptos: builder.query({
      query: (count) => createRequest(`/coins?limit=${count}`),
    }),
    getExchanges: builder.query({
      query: () => createRequest("/coin/Qwsogvtv82FCd/exchanges"),
    }),
    getCryptosDetails: builder.query({
      query: (coinId) => createRequest(`/coin/${coinId}`),
    }),
    getCryptosHistory: builder.query({
      query: ({ coinId, timePeriod }) =>
        //(`/coin/${coinId}/history?timeperiod=${timePeriod}`),
        createTimeperoidRequest(`/coin/${coinId}/history`, {
          timePeriod: timePeriod,
        }),
    }),
  }),
});
//export the data query
export const {
  useGetCryptosQuery,
  useGetCryptosDetailsQuery,
  useGetExchangesQuery,
  useGetCryptosHistoryQuery,
} = cryptoApi;
