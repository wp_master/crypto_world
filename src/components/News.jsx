import React, { useEffect, useState } from "react";
import { Select, Typography, Row, Col, Avatar, Card } from "antd";
import moment from "moment";

import { useGetCryptosNewsQuery } from "../services/cryptoNewsApi";
import { useGetCryptosQuery } from "../services/cryptoApi";
import Loader from "./Loader";
const { Text, Title } = Typography;
const { Option } = Select;

const demoImage =
  "https://www.bing.com/th?id=OVFT.mpzuVZnv8dwIMRfQGPbOPC&pid=News";

const truncate = (str) => {
  return str.length > 46 ? str.substring(0, 46) + "..." : str;
};

const News = ({ simplified }) => {
  const [newsCategory, setNewsCategory] = useState("Cryptocurrency");
  //fetch news data from redux query
  const { data: cryptoNews } = useGetCryptosNewsQuery({
    newsCategory: newsCategory,
    count: simplified ? 6 : 12,
  });

  //get crypto currencies data (100 coins)
  const { data } = useGetCryptosQuery(100);
  //loading mode
  if (!cryptoNews) return <Loader />;

  return (
    <Row gutter={[24, 24]}>
      {!simplified && (
        <Col span={24}>
          <Title level={2} className="heading" style={{ marginBottom: 30 }}>
            Crypto Worwide News
          </Title>
          <Card>
            <Select
              style={{ width: 200 }}
              showSearch
              placeholder="Select a Crypto"
              optionFilterProp="children"
              onChange={(value) => setNewsCategory(value)}
              filterOption={(input, option) =>
                option.children.toLowerCase().indexOf(input.toLowerCase() >= 0)
              }
            >
              <Option value="Cryptocurrency">CryptoCurrency</Option>
              {data?.data?.coins.map((coin) => (
                <Option value={coin.name}>{coin.name}</Option>
              ))}
            </Select>
          </Card>
        </Col>
      )}
      {cryptoNews.value.map((news, i) => (
        <Col xs={24} sm={12} lg={8} key={i}>
          <Card hoverable className="news-card">
            <a href={news.url} target="_blank" rel="noreferrer">
              <div className="news-image-container">
                <Title className="news-title" level={4}>
                  {truncate(news.name)}
                </Title>
                <img
                  src={news?.image?.thumbnail?.contentUrl || demoImage}
                  alt="news_img"
                />
              </div>
              <p>
                {news.description > 50
                  ? `${news.description.substring(0, 50)}...`
                  : news.description}
              </p>
              <div className="provider-container">
                <Avatar
                  src={
                    news.provider[0]?.image?.thumbnail?.contentUrl || demoImage
                  }
                  alt="news"
                />

                <Text className="provider-name">{news.provider[0]?.name}</Text>
                <Text>
                  {moment(news.datePublished).startOf("ss").fromNow()}
                </Text>
              </div>
            </a>
          </Card>
        </Col>
      ))}
    </Row>
  );
};

export default News;
