import React, { useEffect } from "react";
import millify from "millify";
import { Collapse, Row, Col, Typography, Avatar } from "antd";
import { useGetExchangesQuery } from "../services/cryptoApi";
import Loader from "./Loader";

const { Text, Title } = Typography;
const { Panel } = Collapse;

const Exchanges = ({ scrollToTop }) => {
  const { data, isFetching } = useGetExchangesQuery();
  const exchangesList = data?.data?.exchanges;

  useEffect(() => {
    scrollToTop();
  }, []);

  if (isFetching) return <Loader />;

  return (
    <>
      <Title level={2} className="heading" style={{ marginBottom: 30 }}>
        Top 50 Worwide Exchanges
      </Title>
      <Row>
        <Col span={6} className="mb-1">
          Exchanges
        </Col>
        <Col span={6} className="mb-1">
          Markets
        </Col>
        <Col span={6} className="mb-1">
          Price
        </Col>
        <Col span={6} className="mb-1">
          BTC Price
        </Col>
      </Row>
      <Row>
        {exchangesList.map((exchange) => (
          <Col key={exchange.uuid} span={24}>
            <Collapse>
              <Panel
                key={exchange.uuid}
                showArrow={false}
                header={
                  <Row key={exchange.uuid}>
                    <Col span={6}>
                      <Text>
                        <strong>{exchange.rank}.</strong>
                      </Text>
                      <Avatar
                        className="exchange-image"
                        src={exchange.iconUrl}
                      />
                      <Text>
                        <strong>{exchange.name}</strong>
                      </Text>
                    </Col>
                    <Col span={6}>{millify(exchange.numberOfMarkets)}</Col>
                    <Col span={6}>${millify(exchange.price)}</Col>
                    <Col span={6}>{millify(exchange.btcPrice)}</Col>
                  </Row>
                }
              >
                {/*HTMLReactParser(exchange.description || "")*/}
                <a href={exchange.coinrankingUrl} target="blank">
                  More Info
                </a>
              </Panel>
            </Collapse>
          </Col>
        ))}
      </Row>
    </>
  );
};

export default Exchanges;
