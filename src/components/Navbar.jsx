import React from "react";
import { Button, Menu, Typography, Avatar } from "antd";
import { Link } from "react-router-dom";
import {
  HomeOutlined,
  MoneyCollectOutlined,
  BulbOutlined,
  FundOutlined,
  MenuOutlined,
  WindowsFilled,
} from "@ant-design/icons";
import logo from "../assets/images/logo.png";
import { useState, useEffect } from "react";

const Navbar = () => {
  const [activeMenu, setActiveMenu] = useState(true);
  const [screenSize, setScreenSize] = useState(null);

  useEffect(() => {
    const handleResize = () => setScreenSize(window.innerWidth);
    //listener for screen resize
    window.addEventListener("resize", handleResize);
    handleResize();

    return () => WindowsFilled.removeEventListener("resize", handleResize);
  }, []);

  useEffect(() => {
    if (screenSize < 768) {
      //hide menu on tablet and mobile
      setActiveMenu(false);
    } else {
      //show menu for desktop
      setActiveMenu(true);
    }
  }, [screenSize]);

  const handleMenu = () => {
    if (screenSize < 768) {
      //hide menu on tablet and mobile
      setActiveMenu(false);
    } else {
      //show menu for desktop
      setActiveMenu(true);
    }
  };

  return (
    <div className="nav-container">
      <div className="logo-container">
        <Avatar src={logo} size="large" />
        <Typography.Title level={2} className="logo">
          <Link to="/">CryptoWorld</Link>
        </Typography.Title>
        <Button
          className="menu-control-container"
          onClick={() => setActiveMenu(!activeMenu)}
        >
          <MenuOutlined />
        </Button>
      </div>
      {activeMenu && (
        <Menu theme="dark" triggerSubMenuAction="click">
          <Menu.Item key="1" icon={<HomeOutlined />} onClick={handleMenu}>
            <Link to="/">Home</Link>
          </Menu.Item>
          <Menu.Item key="2" icon={<FundOutlined />} onClick={handleMenu}>
            <Link to="/cryptocurrencies">Crypto Currencies</Link>
          </Menu.Item>
          <Menu.Item
            key="3"
            icon={<MoneyCollectOutlined />}
            onClick={handleMenu}
          >
            <Link to="/exchanges">Exchanges</Link>
          </Menu.Item>
          <Menu.Item key="4" icon={<BulbOutlined />} onClick={handleMenu}>
            <Link to="/news">News</Link>
          </Menu.Item>
        </Menu>
      )}
    </div>
  );
};

export default Navbar;
