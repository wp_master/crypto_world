import React, { useEffect, useState } from "react";
import millify from "millify";
import { Link } from "react-router-dom";
import { Card, Row, Col, Input } from "antd";
import { useGetCryptosQuery } from "../services/cryptoApi";
import Loader from "./Loader.jsx";

const Cryptocurrencies = ({ simplified }) => {
  //limit to 10 displayed currencied on default
  const count = simplified ? 10 : 100;
  //destruct the data from redux
  const { data: cryptoslist, isFetching } = useGetCryptosQuery(count);
  const [cryptos, setCryptos] = useState([]);
  const [searchTerm, setSearchTerm] = useState("");

  useEffect(() => {
    //filter serach terms unput
    const filteredData = cryptoslist?.data?.coins.filter((coin) =>
      coin.name.toLowerCase().includes(searchTerm.toLowerCase())
    );
    //result for filtered data
    setCryptos(filteredData);
  }, [cryptoslist, searchTerm]);

  //Laoding mode
  if (isFetching) return <Loader />;

  return (
    <>
      {!simplified && (
        <div className="search-crypto">
          <Input
            placeholder="Search Crypto Currency..."
            onChange={(e) => setSearchTerm(e.target.value)}
          />
        </div>
      )}

      <Row gutter={[32, 32]} className="crypto-card-container">
        {cryptos.length > 0 ? (
          cryptos?.map((currency) => (
            <Col
              xs={24}
              sm={12}
              lg={6}
              className="crypto-card"
              key={currency.uuid}
            >
              <Link to={`/coin/${currency.uuid}`}>
                <Card
                  level={2}
                  title={`${currency.rank}.${currency.name}`}
                  extra={
                    <img
                      className="crypto-image"
                      alt={currency.name}
                      src={currency.iconUrl}
                    />
                  }
                  hoverable
                >
                  <p>Price: {millify(currency.price)}</p>
                  <p>Market Cap: {millify(currency.marketCap)}</p>
                  <p>Daily Change: {millify(currency.change)}%</p>
                </Card>
              </Link>
            </Col>
          ))
        ) : (
          <h2 className="not-found-title">
            Currency not found, Please search again...
          </h2>
        )}
      </Row>
    </>
  );
};

export default Cryptocurrencies;
